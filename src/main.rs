use std::thread;
use std::net::{TcpListener, TcpStream, Shutdown};
use std::io::{Read, Write};
use std::str;

//上面是需要用到的库文件


fn handle_client(mut stream: TcpStream) {
    // 50字节数据缓冲区大小
    let mut data = [0 as u8; 50];
    // 循环获取输入流信息
    while match stream.read(&mut data) {
        // 获取到 stream.read() 的返回值
        Ok(size) => {
            // 将u8数组转换为&str，删除最后一个字符('\x0a')
            let str = match str::from_utf8(&data[0..size - 1]) {
                Ok(v) => v,
                Err(_) => "Invalid UTF-8 sequence received."
            };
            // 打印接受内容
            println!("Message received from {}: {}", stream.peer_addr().unwrap(), str);
            // 写入回显数据返回给连接端
            stream.write(&data[0..size]).unwrap();
            // 成功返回true
            true
        },
        // 处理输入时发生的错误
        Err(_) => {
            // 打印错误信息并关闭连接
            println!("Error occurred while reading from input stream, terminating connection with {}", stream.peer_addr().unwrap());
            // 关闭所有输入信息
            stream.shutdown(Shutdown::Both).unwrap();
            // 返回错误false
            false
        }
    } {}
}

fn main() {
    // 绑定本机端口，开启监听
    let listener = TcpListener::bind("127.0.0.1:18888").unwrap();
    // 打印服务成功运行的说明
    println!("Server listening on port 18888");
    println!("Server successful running");
    // 迭代器迭代循环每一个接入的连接
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                // 服务端显示接入连接信息
                println!("New connection: {}", stream.peer_addr().unwrap());
                // 为接入连接开启新的线程
                thread::spawn(move|| {
                    // 处理传入的信息
                    handle_client(stream)
                });
            }
            Err(e) => {
                // 打印错误信息
                println!("Error: {}", e);
            }
        }
    }
    // 关闭服务
    drop(listener);
}

